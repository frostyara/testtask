﻿using System;

namespace TestTask.Common.Entities
{
    public class CodeChange
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }

        public string Author { get; set; }

        public string Reason { get; set; }

        public string Assembly { get; set; }

        public string Class { get; set; }

        public string Method { get; set; }
    }
}
