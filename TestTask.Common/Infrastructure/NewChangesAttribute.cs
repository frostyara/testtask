using System;

namespace TestTask.Common.Infrastructure
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class NewChangesAttribute : Attribute
    {
        public DateTime DateModify { get; set; }

        public string Reason { get; set; }

        public string Author { get; set; }

        public NewChangesAttribute(string dateModify, string author, string reason)
        {
            DateModify = DateTime.Parse(dateModify);
            Reason = reason;
            Author = author;
        }
    }
}
