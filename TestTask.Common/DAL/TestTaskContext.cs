﻿using System.Data.Entity;
using TestTask.Common.Entities;

namespace TestTask.Common.DAL
{
    public class TestTaskContext : DbContext
    {
        public DbSet<CodeChange> CodeChanges { get; set; }
    }
}
