﻿using System;
using TestTask.Common.DAL;

namespace TestTask.CodeChangeHistory
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new TestTaskContext())
            {
                foreach (var change in ctx.CodeChanges)
                {
                    Console.WriteLine("{0} {1} {2} {3} {4} {5}", change.Date, change.Author, change.Reason,
                        change.Assembly, change.Class, change.Method);
                    Console.WriteLine();
                }
            }

            Console.WriteLine();
            Console.WriteLine("Press any key");

            Console.ReadKey();
        }
    }
}
