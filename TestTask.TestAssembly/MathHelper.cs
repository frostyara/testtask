﻿using System;
using TestTask.Common.Infrastructure;

namespace TestTask.TestAssembly
{
    internal class MathHelper
    {
        [NewChanges("09.04.2015 13:51", "Andrew", "Indicates if a given integer is prime")]
        private int Sqrt(int n)
        {
            return (int) Math.Sqrt(n);
        }
    }
}
