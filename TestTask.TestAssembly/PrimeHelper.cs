﻿using System;
using TestTask.Common.Infrastructure;

namespace TestTask.TestAssembly
{
    [NewChanges("09.04.2015 13:49", "Andrew", "I added a new class with \"PrimeHelper\" name")]
    public static class PrimeHelper
    {
        [NewChanges("09.04.2015 13:51", "Andrew", "Indicates if a given integer is prime")]
        [NewChanges("09.04.2015 13:52", "Andrew", "Make some optimization")]
        public static bool IsPrime(int n)
        {            
            var sqrt = (int)Math.Sqrt(n);
            for (var i = 2; i <= sqrt; i++)
            {
                if (n%i == 0)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
