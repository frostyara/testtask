﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TestTask.Common.DAL;
using TestTask.Common.Entities;
using TestTask.Common.Infrastructure;

namespace TestTask.Analyzer
{
    class Program
    {
        private static readonly List<CodeChange> changes = new List<CodeChange>(); 

        static void Main(string[] args)
        {
            var assembly = Assembly.LoadFrom(String.Join(" ", args));
            var types = assembly.GetTypes();
            using (var ctx = new TestTaskContext())
            {
                foreach (var type in types)
                {
                    if (type.IsClass)
                    {
                        var classAttrs = type.GetCustomAttributes(typeof (NewChangesAttribute));
                        UpdateDb(ctx, assembly.FullName, type.FullName, String.Empty, classAttrs);

                        var methods = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                        foreach (var method in methods)
                        {
                            var methodAttrs = method.GetCustomAttributes(typeof (NewChangesAttribute));
                            UpdateDb(ctx, assembly.FullName, type.FullName, method.Name, methodAttrs);
                        }
                    }
                }

                ctx.SaveChanges();
            }
        }

        private static void UpdateDb(TestTaskContext ctx, string assemblyName, string className, string methodName, IEnumerable<Attribute> attrs)
        {
            var lastModify = LastChange(ctx, assemblyName, className, methodName);
            foreach (var attr in attrs.OfType<NewChangesAttribute>())
            {
                if (attr.DateModify > lastModify)
                {
                    var newCodeChange = new CodeChange
                    {
                        Author = attr.Author,
                        Date = attr.DateModify,
                        Reason = attr.Reason,
                        Assembly = assemblyName,
                        Class = className,
                        Method = methodName
                    };

                    ctx.CodeChanges.Add(newCodeChange);
                }
            }
        }

        static DateTime LastChange(TestTaskContext ctx, string assemblyName, string className, string methodName)
        {
            var query =
                ctx.CodeChanges.Where(c => c.Assembly == assemblyName && c.Class == className && c.Method == methodName)
                    .Select(c => c.Date);
            if (!query.Any())
            {
                return DateTime.MinValue;
            }
            return query.Max(c => c);
        }                     
    }
}
